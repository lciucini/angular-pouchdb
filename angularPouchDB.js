angular.module('pouchDB', [])
  /*
  * puchDB factory to save object in IndexDB
  * Require library puchDB  https://github.com/pouchdb/pouchdb/releases/download/5.2.1/pouchdb-5.2.1.min.js
  */
  /*
  * Methods initDB, put , get, update, destroy
  */
  .factory('pouchDB', function ($q) {
    var db;
    /*
    * Prepares data for the request
    * @id     {string/int}        only reference id
    * @title  {title of object}   title for object
    * @data   {JSONobject}        Object to save
    */
    function transformRequest(id, title, data) {
      var dataSave    = {};
      dataSave._id    = id;
      dataSave.title  = title;

      angular.forEach(data, function (item, key) {
        dataSave[key] = item;
      })
      return dataSave;
    }

    return {
      /*
      * create databases or define the database if it exists
      * dbName {string} Name to database
      */
      initDB: function (dbName) {
        db = new PouchDB(dbName);
      },
      /*
      * Put an object in the data base
      * @id     {string/int}  only reference id
      * @title  {string}      title for object
      * @data   {JONSobject}  Object to save
      */
      put: function (id, title, data) {
        var deferred = $q.defer();
        var dataSave = transformRequest(id, title, data);

        db.put(dataSave).then(function (result) {
          deferred.resolve(result);
        }).catch(function (error) {
          deferred.reject(error);
        });
        return deferred.promise;
      },
      /*
      * Get object in data base
      * @id     {string/int}  id to get
      */
      get: function (id) {
        var deferred = $q.defer();

        db.get(id).then(function (result) {
          deferred.resolve(result);
        }).catch(function (error) {
          deferred.reject(error);
        })
        return deferred.promise;
      },
      /*
      * Update an object in the data base
      * @id     {string/int}  only reference id
      * @title  {string}      title for object
      * @data   {JONSobject}  Object to save
      */
      update: function (id, title, data) {
        var deferred  = $q.defer();
        var dataSave  = transformRequest(id, title, data);

        db.get(id).then(function(doc) {
          dataSave._rev = doc._rev;

          return  db.put(dataSave)
        }).then(function(response) {
          deferred.resolve(response);
        }).catch(function (error) {
          deferred.reject(error);
        });
        return deferred.promise;
      },
      /*
      * Destroy database
      */
      destroy: function () {
        var deferred = $q.defer();

        db.destroy().then(function (result) {
          deferred.resolve(result)
        }).catch(function (error) {
          deferred.reject(error)
        });
        return deferred.promise;
      }
    }
  });
