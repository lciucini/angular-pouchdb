# Angular Module fot Pouch DB (v 1.0.0)
Angular factory to library Pouch DB

## Install 

### Add pouch DB library in yout index HTML
```HTML
    <script src="lib/pouchdb-5.2.1.min.js"></script>
```

### Add angular module in your index HTML
```HTML
    <script src="lib/angularPouchDB.js"></script>
```

### Add angular module in your app.js
```javascript
    angular.module('app', ['pouchDB'])  
```    
## Usage
Add dependency in your controller

```javascript
.controller('home', function($scope, pouchDB) {})
```
Example for implementations
```javascript
controllers.controller('home', function($scope, pouchDB) {
    $scope.data = {};

    var exampleObject = {
      value1: 'value',
      value2: 'value'
    }

    /*
    * Init Data Base
    * @nameDB {string}
    * */
    $scope.initDB = function () {
      pouchDB.initDB($scope.data.nameDB);
    }

    /*
    * Put Object in data base
    * @id     {string || int} only identifier
    * @title  {string}*       title of object
    * @object {JSONobject}    object to save
    * */
    $scope.put = function () {
      pouchDB.put($scope.data.id, $scope.data.name, exampleObject).then(function (result) {
        console.log(result);
      }, function (error) {
        console.log(error);
      })
    }

    /*
    * get Object in data base
    * @id     {string || int} only identifier
    *
    */
    $scope.get = function () {
      pouchDB.get($scope.data.id).then(function (data) {
        $scope.result = data;
      }).catch(function (error) {
        console.log(error);
      })
    }

    /*
    * Update Object in data base
    * @id     {string || int} only identifier
    * @title  {string}*       title of object
    * @object {JSONobject}    object to save
    */
    $scope.update = function () {
      pouchDB.update($scope.data.id, $scope.data.name, exampleObject).then(function (result) {
        console.log(result);
      }, function (error) {
        console.log(error);
      })
    }

    /*
    * Destroy database
    */
    $scope.destroy = function () {
      pouchDB.destroy();
    };
})
```